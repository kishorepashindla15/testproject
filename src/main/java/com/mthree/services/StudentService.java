package com.mthree.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mthree.models.Student;
import com.mthree.repositories.StudentRepository;

@Service
public class StudentService {
	
	// Business methods / Business logic 
	
	@Autowired
	private StudentRepository studentRepository;
	
	public Student saveStudentObject(Student s) {
		return studentRepository.save(s);
	}
	
	
	public List<Student> loadAllStudents() {
		return studentRepository.findAll();
	}
	
	
	public List<Object> getAllStudentsAndCourses(){
		return studentRepository.getStudentsAlongWithCourse();
	}


	public List<Object> loadStudentsByName(String name) {
		// TODO Auto-generated method stub
		return studentRepository.getStudentsAlongWithCourse();
	}
	
	

}
