package com.mthree.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Course implements Serializable {
	
	@Id
	private int courseID;
	
	private String courseTitle;
	
	@ManyToOne
	@JoinColumn(name="student_id")
//	@Fetch(FetchMode.JOIN)
	private Student id;
	
	
	public Course() {}

	

	public Course(int courseID, String courseTitle, Student student) {
		super();
		this.courseID = courseID;
		this.courseTitle = courseTitle;
		this.id = student;
	}



	public int getCourseID() {
		return courseID;
	}

	public void setCourseID(int courseID) {
		this.courseID = courseID;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}


	public Student getStudent() {
		return id;
	}

	public void setStudent(Student student) {
		this.id = student;
	}



	@Override
	public String toString() {
		return "Course [courseID=" + courseID + ", courseTitle=" + courseTitle + ", student=" + id + "]";
	}




}
