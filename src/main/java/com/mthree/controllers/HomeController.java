package com.mthree.controllers;

import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mthree.models.Student;
import com.mthree.services.StudentService;

@Controller
public class HomeController {
	
	@Autowired
	private StudentService studentService;
	
	// Write all request mapping methods
	
	@RequestMapping("/")
	public String home() {
		return "home"; // This string value represent a view name (jsp file name)
	}
	
	
//	@RequestMapping("/hi")
//	public void sayHello(HttpServletRequest request,HttpServletResponse response) throws Exception {
//		
//		PrintWriter pw = response.getWriter();
//		
//		pw.println("Hello!!");
//		
//	}
//	
//	@RequestMapping(path="/register",method=RequestMethod.GET)
//	public String registerProcess(@RequestParam("name") String name,@RequestParam("email") String email) {
//		return "home";
//	}
//	
//	//http://localhost:8080/register2?id=1&name=Kishore
//	//http://localhost:8080/register2?id2=1&namedemo=Kishore
//	@RequestMapping(path="/register2",method=RequestMethod.GET)
//	public String registerProcess2(@ModelAttribute Student s) {
//		System.out.println(s.getId()+"  "+s.getName());
//		return "home";
//	}
//	
//	
//	@RequestMapping(path="/register3",method=RequestMethod.GET)
//	public ModelAndView registerProcess3(@ModelAttribute Student s) {
//		System.out.println(s.getId()+"  "+s.getName());
//		ModelAndView mv = new ModelAndView("welcome","s1",s);
//		mv.addObject("today",LocalDateTime.now());
//		return mv;
//	}
//		
//	@RequestMapping(path="/register4",method=RequestMethod.GET)
//	public ModelAndView registerProcess4(@ModelAttribute Student s) {
//		
//		Student persistedStudentObj = studentService.saveStudentObject(s);
//		ModelAndView mv = null;
//		if(persistedStudentObj!=null) {
//			 mv = new ModelAndView("welcome","s1",s);
//			 mv.addObject("today",LocalDateTime.now());
//		}
//		else {
//			mv = new ModelAndView("welcome","s1","Unable to register, please try again!!");
//			mv.addObject("today",LocalDateTime.now());
//		}
//		return mv;
//	}
//	
	

}
