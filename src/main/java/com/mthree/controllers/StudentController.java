package com.mthree.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mthree.models.Student;
import com.mthree.services.StudentService;

@RestController
public class StudentController {
	
	
	@Autowired
	private StudentService studentService;
	
	
	
	// Rest API end-points 
//	@RequestMapping("/getAllStudents")
//	public List<Student> getAllStudents(){
//		
//		List<Student> students = new ArrayList<>();
//		students.add(new Student(1,"Kishore"));
//		students.add(new Student(2,"Bob"));
//		students.add(new Student(3,"John"));
//		
//		return students;
//	}
	
	@RequestMapping("/allStudents")
	public List<Student> loadStudents(){
				
		return studentService.loadAllStudents();
	}
		
	@GetMapping("/studentsWithCourses")
	public List<Object> studentsAndCourse(){
		return studentService.getAllStudentsAndCourses();
	}
	
	
	@PostMapping("/fetchStudentByName")
	public List<Object> fetchStudentByName(@Param("name") String name){
		return studentService.loadStudentsByName(name);
	}
	
	
	
}
