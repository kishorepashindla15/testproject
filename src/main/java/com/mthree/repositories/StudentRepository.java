package com.mthree.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mthree.models.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {
	
	// User- Defined methods using Patterns
	
	public List<Student> findStudentByName(String name);
	
	public List<Student> findStudentByNameLike(String name);
	// Here Student is name of a class and name is a field(property)
	// :name is a placeholder which would be replaced with the actual input value
	// To substitute a value into the placeholder use @Param annotation
	@Query(value="SELECT s from Student s WHERE s.name = :name")
	public List<Object> getAllStudentsByName(@Param("name") String name);
	
	// An example of Native query (Standard SQL query)
	@Query(value="SELECT * from student WHERE name = :name",nativeQuery=true)
	public List<Student> getAllStudentsByName2(@Param("name") String name);
	
	// Use table aliases to access the column names
	@Query(value="SELECT s.id,s.name,c.courseTitle from Student s INNER JOIN s.courses c ON s.id = c.id")
	public List<Object> getStudentsAlongWithCourse();
	
	
	

}
