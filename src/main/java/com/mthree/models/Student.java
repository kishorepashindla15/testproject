package com.mthree.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class Student implements Serializable{
	
	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	//@Column(name="student_name")
	private String name;
	
	@OneToMany(targetEntity=Course.class,fetch=FetchType.LAZY,mappedBy="id")
	private Set<Course> courses;
	
	public Student() {}
	
	
	
	
	public Student(int id, String name, Set<Course> courses) {
		super();
		this.id = id;
		this.name = name;
		this.courses = courses;
	}




	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Course> getCourses() {
		return courses;
	}




	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}




	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", courses=" + courses + "]";
	}

	
	

}
